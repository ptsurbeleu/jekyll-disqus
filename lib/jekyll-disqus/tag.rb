require 'jekyll'
require 'liquid'

module Jekyll
  module Disqus
    class DisqusTag < Liquid::Tag
      # use Liquid extensions to lookup the variable
      include Jekyll::LiquidExtensions

      # need a wrapper to return nil in case variable is not found
      def eval(context, variable)
        # try variable lookup first
        output = lookup_variable(context, variable)
        # return value only if the variable is found (note the use of identity comparison operator)
        output unless output === variable
      end

      def render(context)
        @show_comments = eval(context, 'page.comments')
        @shortname = eval(context, 'site.disqus_shortname')
        # site url, eq. http://pabloduo.com
        @site_url = eval(context, 'site.url')
        # page url, eq. /2017/02/13/about-vim.html
        @page_url = eval(context, 'page.url')
        # page identifier, eq. /2017/02/13/about-vim.html
        @page_id = eval(context, 'page.url') || eval(context, 'page.name')
        # absolute url to the page, eq. http://pabloduo.com/2017/02/13/about-vim.html
        @absolute_page_url = @site_url + @page_url

        universal_code = <<-DISQUS
          <div id="disqus_thread"></div>
          <script>
              /**
              *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
              *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
              */

              var disqus_config = function () {
                  this.page.url = "#{@absolute_page_url}";  // Replace PAGE_URL with your page's canonical URL variable
                  this.page.identifier = "#{@page_id}";     // Replace PAGE_IDENTIFIER with your page's unique identifier variable
              };

              (function() { // DON'T EDIT BELOW THIS LINE
                  var d = document, s = d.createElement('script');
                  s.src = 'https://#{@shortname}.disqus.com/embed.js';
                  s.setAttribute('data-timestamp', + new Date());
                  (d.head || d.body).appendChild(s);
              })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        DISQUS
        universal_code if @show_comments === true
      end
    end
  end
end

# register a new liquid tag
Liquid::Template.register_tag('disqus', Jekyll::Disqus::DisqusTag)