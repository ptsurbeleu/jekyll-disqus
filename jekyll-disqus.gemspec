# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll-disqus/version'

Gem::Specification.new do |spec|
  spec.name          = "jekyll-disqus"
  spec.version       = Jekyll::Disqus::VERSION
  spec.authors       = ["Pavel Tsurbeleu"]
  spec.email         = ["pavel.tsurbeleu@me.com"]

  spec.summary       = %q{Summary: This gem enables Disqus in your Jekyll site with a simple custom tag.}
  spec.description   = %q{This gem enables Disqus in your Jekyll site with a simple custom tag.}
  spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "jekyll", "~>3.4"
  spec.add_development_dependency "liquid", "~>3.0"
  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
end
